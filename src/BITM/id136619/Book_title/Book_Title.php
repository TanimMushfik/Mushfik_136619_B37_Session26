<?php


namespace App\Book_title;
use App\Message\Message;
use App\Utility\Utility;
use App\Database as DB;

use PDO;

class Book_Title extends DB
{
    public $id;

    public $book_title;

    public $author_name;

    public function __construct()
    {

        parent::__construct();

    }
    public function setData($data){

        if(array_key_exists('id',$data)) {
            $this->id = $data['id'];
        }
         if(array_key_exists('book_title',$data)) {
            $this->book_title = $data['book_title'];
        }
         if(array_key_exists('author_name',$data)) {
            $this->author_name = $data['author_name'];
        }
    }

    public function store(){
      $DBH=$this->conn;
        $data = array('book_title'=>$this->book_title,'author_name'=>$this->author_name);
      $STH =  $DBH->prepare("insert into `book_title` (`book_title`,`author_name`) VALUES (:book_title,:author_name)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ book_title: $this->book_title ] , [ author_name: $this->author_name ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }



    public function index($mode="ASSOC"){
       $mode = strtoupper($mode);
        $STH = $this->conn->query('SELECT * from book_title');


        if($mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }




}
