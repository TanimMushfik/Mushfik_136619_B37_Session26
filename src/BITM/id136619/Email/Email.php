<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/1/2016
 * Time: 6:32 PM
 */

namespace App\Email;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class Email extends DB
{
    public $id;

    public $name;

    public $email;


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data){

        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }

    }


    public function store()
    {
        $DBH = $this->conn;
        $data = array('name' => $this->name,'email' => $this->email);
        $STH = $DBH->prepare("insert into `email` (`name`,`email`) VALUES (:name,:email)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ],[ email: $this->email ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }
}