<?php
require_once("../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<div id =\"message\">". Message::message()."</div>";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../../resource/css/style.css">

    <link rel="stylesheet" href="../../resource/bootstrap/css/bootstrap.min.css">
</head>
<body>
<h2>Gender</h2>
<form class="form-horizontal" method="post" action="store.php">

    <div class="form-group">
        <label class="control-label col-sm-2" for="pwd">Gender:</label>
        <div class="col-sm-4">
            <div class="radio">
                <label><input type="radio" name="gender" value="male">Male</label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" value="female">Female</label>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" name="submit" class="btn btn-info">Submit</button>
        </div>
    </div>
</form>

<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>
</body>
</html>




