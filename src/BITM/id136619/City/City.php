<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/1/2016
 * Time: 6:32 PM
 */

namespace App\City;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;


class City extends DB
{
    public $id;

    public $city;

    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data)
    {
        if (array_key_exists('city', $data)) {
            $this->city = $data['city'];
        }
    }

    public function store()
    {
        $DBH = $this->conn;
        $data = array('city' => $this->city);
        $STH = $DBH->prepare("insert into `city` (`city`) VALUES (:city)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ city: $this->city ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }
}