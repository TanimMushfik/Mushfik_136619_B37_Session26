<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/1/2016
 * Time: 6:33 PM
 */

namespace App\Profile_Picture;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;


use PDO;

class Profile_Picture extends DB
{
    public $id;

    public $name;

    public $profile_picture;


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data)
    {
        if (array_key_exists('file', $data)) {
            $this->picture = $data['file'];
        }
    }


    public function store()
    {
        $DBH = $this->conn;
        $data = array($this->profile_picture);
        $STH = $DBH->prepare("INSERT INTO `profile_picture`(`profile_picture`) VALUES (:profile_picture)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[profile_picture: $this->profile_picture] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');
    }
}