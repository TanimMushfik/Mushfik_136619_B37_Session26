<?php


namespace App\Summary_Of_Organization;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class Summary_Of_Organization extends DB
{
    public $id;

    public $summary;

    public $organization;


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data){


        if(array_key_exists('summary',$data)) {
            $this->summary = $data['summary'];
        }
        if(array_key_exists('organization',$data)) {
            $this->organization = $data['organization'];
        }
    }

    public function store(){
        $DBH=$this->conn;
        $data = array('summary'=>$this->summary,'organization'=>$this->organization);
        $STH =  $DBH->prepare("insert into `summary_of_organization` (`summary`,`organization`) VALUES (:summary,:organization)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ summary: $this->summary ] , [ organization: $this->organization ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }

}