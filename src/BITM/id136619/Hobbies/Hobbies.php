<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 11/1/2016
 * Time: 6:33 PM
 */

namespace App\Hobbies;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;

use PDO;

class Hobbies extends DB
{
    public $id;

    public $hobbies;


    public function __construct()
    {

        parent::__construct();

    }

    public function setData($data){

        if(array_key_exists('hobbies',$data)){
            $hobby = implode(',',$data['hobbies']);
            $this->hobbies = $hobby;
        }

    }


    public function store()
    {

        $checkBox = implode(',', $_POST['hobbies']);
        $DBH = $this->conn;
        $data = array('hobbies' => $this->hobbies);
        $STH = $DBH->prepare("insert into `hobbies` (`hobbies`) VALUES (:hobbies)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ hobbies: $this->hobbies ]<br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }
}