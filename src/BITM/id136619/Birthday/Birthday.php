<?php


namespace App\Birthday;
use App\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Birthday extends DB
{
    public $id;

    public $name;

    public $birthday_date;



    public function __construct(){

        parent::__construct();

    }

    public function setData($data){

        if(array_key_exists('name',$data)) {
            $this->name= $data['name'];
        }
        if(array_key_exists('birthday_date',$data)) {
            $this->birthday_date = $data['birthday_date'];
        }
    }

    public function store(){
        $DBH=$this->conn;
        $data = array('name'=>$this->name,'birthday_date'=>$this->birthday_date);
        $STH =  $DBH->prepare("insert into `birthday` (`name`,`birthday_date`) VALUES (:name ,:birthday_date)");

        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ name: $this->name ] , [ birthday_date: $this->birthday_date ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');

    }
}


